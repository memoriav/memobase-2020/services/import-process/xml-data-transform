/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.BranchNames.OAI_CONFIG_ISSUE
import ch.memobase.BranchNames.SFTP_CONFIG_ISSUE
import ch.memobase.kafka.utils.ConfigJoiner
import ch.memobase.kafka.utils.models.ImportService
import ch.memobase.settings.SettingsLoader
import ch.memobase.sftp.SftpClient
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.Topology
import java.io.File

class KafkaTopology(private val settings: SettingsLoader) {
    private val step = settings.appSettings.getProperty(PropertyName.REPORTING_STEP_NAME)
    private val stepVersion = settings.appSettings.getProperty(PropertyName.APP_VERSION)
    private val reports = Reports(step, stepVersion)
    private val sftpClient: SftpClient = SftpClient(settings.sftpSettings)
    private val xsltHandler = XsltHandler(reports)
    private val configJoiner = ConfigJoiner(
        ImportService.Transform, Serdes.String(), Serdes.ByteArray(), xsltHandler::xsltFunction,
    )

    private val inputTopic = settings.inputTopic
    private val oaiInputTopic = settings.appSettings.getProperty(PropertyName.OAI_INPUT_TOPIC)
    private val outputTopic = settings.outputTopic
    private val reportingTopic = settings.processReportTopic

    private val pipeline = PipelineBuilder()

    fun build(): Topology {
        val configStream = pipeline
            .configurationStream(settings.appSettings.getProperty(PropertyName.CONFIG_TOPIC))

        // OaiPipeline
        pipeline
            .inputStream(oaiInputTopic)
            .joinConfiguration(configJoiner, configStream, reports, reportingTopic, OAI_CONFIG_ISSUE)
            ?.retrieveHeaderMetadata()
            ?.mapValues { value ->
                InputStreamOutput(
                    value.content.byteInputStream(),
                    value.content,
                    value.configuration,
                    value.headerMetadata
                )
            }
            ?.applyXslt(xsltHandler)
            ?.publishResults(outputTopic, reportingTopic)

        // SftpPipeline
        pipeline
            .inputStream(inputTopic)
            .filterForXMLMessages()
            .joinConfiguration(configJoiner, configStream, reports, reportingTopic, SFTP_CONFIG_ISSUE)
            ?.retrieveHeaderMetadata()
            ?.mapValues { value ->
                InputStreamOutput(
                    sftpClient.open(File(value.content)), value.content, value.configuration, value.headerMetadata
                )
            }
            ?.applyXslt(xsltHandler)
            ?.publishResults(outputTopic, reportingTopic)


        return pipeline.build()
    }
}
