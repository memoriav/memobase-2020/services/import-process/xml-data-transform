/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.BranchNames.CONFIG_ISSUE_NO
import ch.memobase.BranchNames.CONFIG_ISSUE_NULL
import ch.memobase.BranchNames.CONFIG_ISSUE_YES
import ch.memobase.kafka.utils.ConfigJoiner
import ch.memobase.reporting.ReportStatus
import ch.memobase.settings.HeaderExtractionSupplier
import ch.memobase.utility.AcceptedFileFormat
import ch.memobase.utility.TextFileValidationMessage
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Branched
import org.apache.kafka.streams.kstream.Consumed
import org.apache.kafka.streams.kstream.KStream
import org.apache.kafka.streams.kstream.Named

class PipelineBuilder {
    private val builder = StreamsBuilder()

    fun configurationStream(topic: String): KStream<ByteArray, ByteArray> {
        return builder
            .stream(topic, Consumed.with(Serdes.ByteArray(), Serdes.ByteArray()))
    }

    fun inputStream(topic: String): KStream<String, String> {
        return builder.stream(topic)
    }

    fun build(): Topology {
        return builder.build()
    }
}


/**
 * Filter messages to only keep XML messages. And only keep the path of the message.
 */
fun KStream<String, String>.filterForXMLMessages(): KStream<String, String> {
    return this.flatMapValues { _, value -> TextFileValidationMessage.fromJson(value) }
        .filter { _, value -> value.format == AcceptedFileFormat.XML }
        .mapValues { value -> value.path }
}

/**
 * Join the configuration stream with the input stream and handle potential exceptions.
 * @param configJoiner The ConfigJoiner to use for the join
 * @param configStream The configuration stream
 * @param reports Class to generate reports
 * @param reportingTopic The topic to send reports to
 * @return A KStream with the joined configuration and the data stream
 */
fun KStream<String, String>.joinConfiguration(
    configJoiner: ConfigJoiner<String, ByteArray>,
    configStream: KStream<ByteArray, ByteArray>,
    reports: Reports,
    reportingTopic: String,
    namedAs: String,
): KStream<String, JoinConfigurationOutput>? {
    val joinedStream = configJoiner.join(this, configStream)
        .split(Named.`as`("$namedAs-"))
        .branch(
            { _, value -> value.hasException() },
            Branched.`as`(CONFIG_ISSUE_YES)
        )
        .branch(
            { _, value -> value.value.right == null || value.value.right.isEmpty() },
            Branched.`as`(CONFIG_ISSUE_NULL)
        )
        .defaultBranch(Branched.`as`(CONFIG_ISSUE_NO))

    joinedStream["$namedAs-$CONFIG_ISSUE_YES"]
        ?.mapValues { value -> value.exception }
        ?.mapValues { key, value ->
            reports.fatal(key, "Error: No matching XSLT configuration found; ${value.localizedMessage}.")
        }
        ?.mapValues { value -> value.toJson() }
        ?.to(reportingTopic)

    joinedStream["$namedAs-$CONFIG_ISSUE_NULL"]
        ?.mapValues { readOnlyKey, _ ->
            reports.fatal(readOnlyKey, "Error: Matched XSLT configuration was empty.")
        }
        ?.mapValues { value -> value.toJson() }
        ?.to(reportingTopic)

    return joinedStream["$namedAs-$CONFIG_ISSUE_NO"]
        ?.mapValues { value -> value.value }
        ?.mapValues { value ->
            val values = JoinConfigurationOutput(value.left, value.right)
            values
        }
}

/**
 * Extract the header metadata from the input stream and return it together with the configuration and the data stream.
 * @return A KStream with the header metadata, the configuration and the data stream
 */
fun KStream<String, JoinConfigurationOutput>.retrieveHeaderMetadata(): KStream<String, RetrieveHeaderMetadataOutput> {
    return this.processValues(HeaderExtractionSupplier<JoinConfigurationOutput>())
        .mapValues { value ->
            RetrieveHeaderMetadataOutput(
                value.first.content, value.first.configuration, value.second
            )
        }
}

/**
 * Apply the XSLT transformation to the input stream and return the result.
 */
fun KStream<String, InputStreamOutput>.applyXslt(xsltHandler: XsltHandler): KStream<String, XsltOutput> {
    return this.flatMapValues { readOnlyKey, value ->
        xsltHandler.apply(
            readOnlyKey, value.headerMetadata, value.content, value.originalContent, value.configuration,
        )
    }
}

/**
 * Publish the results to the output topic and send reports to the reporting topic.
 */
fun KStream<String, XsltOutput>.publishResults(outputTopic: String, reportingTopic: String) {
    this.filter { _, value -> value.report.status != ReportStatus.fatal }
        .map { _, value -> KeyValue(value.key, value.output) }
        .to(outputTopic)

    this.mapValues { value -> value.report.toJson() }
        .to(reportingTopic)
}