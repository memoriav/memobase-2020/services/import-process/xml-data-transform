/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.settings.HeaderMetadata
import net.sf.saxon.s9api.*
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.message.ObjectMessage
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.charset.Charset
import javax.xml.transform.stream.StreamSource

class XsltHandler(private val reports: Reports) {
    private val log = LogManager.getLogger(this::class.java)
    private val processor = Processor(false)

    fun xsltFunction(xsltData: ByteArray): ByteArray {
        return xsltData
    }

    fun apply(
        key: String, headers: HeaderMetadata, data: InputStream, originalData: String, xsltFile: ByteArray
    ): Iterable<XsltOutput> {
        return data.use { inputStream ->
            try {
                perform(key, headers, inputStream, xsltFile)
            } catch (ex: XsltException) {
                log.error(ObjectMessage(ex))
                listOf(
                    XsltOutput(
                        key, "",
                        reports.fatal(
                            key, "XSLT EXCEPTION: ${ex.localizedMessage}.\n" +
                                    "$originalData\n" +
                                    xsltFile.toString(
                                        Charset.defaultCharset()
                                    )
                        )
                    )
                )
            } catch (ex: MissingIdentifierException) {
                log.error(ObjectMessage(ex))
                listOf(
                    XsltOutput(
                        key, "", reports.fatal(
                            key, "Missing Identifier: ${ex.localizedMessage}.\n$originalData\n${
                                xsltFile.toString(
                                    Charset.defaultCharset()
                                )
                            }"
                        )
                    )
                )
            } catch (ex: SaxonApiException) {
                log.error(ObjectMessage(ex))
                listOf(
                    XsltOutput(
                        key, "",
                        reports.fatal(
                            key, "Semantic XSLT issue (SAXON API EXCEPTION): ${ex.localizedMessage}.\n" +
                                    "$originalData\n" +
                                    xsltFile.toString(
                                        Charset.defaultCharset()
                                    )
                        )
                    )
                )
            }
        }
    }

    private fun perform(
        key: String, headers: HeaderMetadata, data: InputStream, xsltFile: ByteArray
    ): List<XsltOutput> {
        val contentHandler = SAXContentHandler(key, headers.xmlIdentifierFieldName, headers.xmlRecordTag, reports)
        val additionalContentHandlers = mutableListOf<SAXContentHandler>()
        val transformer = compileXslt(xsltFile).load()
        with(transformer) {
            baseOutputURI = "file:///tmp/"
            destination = SAXDestination(contentHandler)
            setResultDocumentHandler {
                val additionalContentHandler =
                    SAXContentHandler(key, headers.xmlIdentifierFieldName, headers.xmlRecordTag, reports)
                additionalContentHandlers.add(additionalContentHandler)
                SAXDestination(additionalContentHandler)
            }
            data.use {
                setSource(StreamSource(it))
                transform()
            }
        }

        if (additionalContentHandlers.isNotEmpty()) {
            val output = mutableListOf<XsltOutput>()
            for (additionalContentHandler in additionalContentHandlers) {
                val identifier = additionalContentHandler.identifier
                if (identifier == null) {
                    throw MissingIdentifierException(
                        "Missing identifier for additional content handler",
                        headers.xmlIdentifierFieldName
                    )
                }

                output.add(
                    XsltOutput(
                        identifier,
                        additionalContentHandler.output.toString(),
                        additionalContentHandler.getReport()
                    )
                )
            }
            return output
        }

        val identifier = contentHandler.identifier
        if (identifier == null) {
            throw MissingIdentifierException("Missing identifier for content handler", headers.xmlIdentifierFieldName)
        }
        return listOf(
            XsltOutput(
                identifier,
                contentHandler.output.toString(),
                contentHandler.getReport()
            )
        )
    }

    private fun compileXslt(xsltFile: ByteArray): XsltExecutable {
        val errorList = mutableListOf<XmlProcessingError>()
        val xsltCompiler = processor.newXsltCompiler().apply {
            setErrorList(errorList)
        }
        val source = StreamSource(ByteArrayInputStream(xsltFile))
        val executable = xsltCompiler.compile(source)
        if (errorList.isNotEmpty()) {
            throw XsltException(errorList)
        }
        return executable
    }
}