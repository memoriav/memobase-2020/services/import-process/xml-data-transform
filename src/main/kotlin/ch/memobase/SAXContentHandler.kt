/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.reporting.Report
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.*
import org.apache.logging.log4j.LogManager
import org.xml.sax.Attributes
import org.xml.sax.ContentHandler
import org.xml.sax.Locator
import java.io.StringWriter

/**
 * Class to transform a xml stream into a json representation. Expects a flat xml preprocessed with
 * a xslt if necessary.
 *
 * Can only handle elements up to one level deep and ignores attributes.
 *
 * @param key The key of the kafka message.
 * @param identifierFieldName The field name of the unique identifier of this record.
 * @param recordTag The root tag of the xml structure.
 */
class SAXContentHandler(
    private val key: String,
    private val identifierFieldName: String,
    private val recordTag: String,
    private val reports: Reports
) : ContentHandler {
    private val log = LogManager.getLogger(this::class.java)

    /**
     * The json representation of the xml stream after processing.
     */
    val output = StringWriter()

    /**
     * The identifier is used as a message key for the outgoing message.
     */
    var identifier: String? = null
    private var report: Report? = null
    private val jsonResult = mutableMapOf<String, Any>()

    /**
     * @return A report on the status of the transformation.
     */
    fun getReport(): Report {
        return report ?: reports.fatal(identifier ?: key, "Unknown Failure: No report found.")
    }

    private var currentElementTag: String = ""
    private var currentInnerElementTag: String = ""
    private var currentElementContent: String = ""
    private var currentInnerElementContent: String = ""
    private val innerElements = mutableListOf<Pair<String, String>>()
    private val invalidPropertyNameCharacters = listOf('.', '+', ',', '\\')
    private var reportText = ""

    override fun setDocumentLocator(p0: Locator?) {
        // NOT NEEDED...
    }

    override fun startDocument() {
        // Do nothing...
    }

    override fun processingInstruction(p0: String?, p1: String?) {
        // NOT NEEDED
    }

    override fun skippedEntity(p0: String?) {
        // NOT NEEDED
    }

    override fun ignorableWhitespace(p0: CharArray?, p1: Int, p2: Int) {
        // Do Nothing
    }

    override fun startPrefixMapping(p0: String?, p1: String?) {
        // NOT NEEDED
    }

    override fun endPrefixMapping(p0: String?) {
        // NOT NEEDED.
    }

    override fun characters(characters: CharArray?, start: Int, size: Int) {
        if (characters == null) return

        val line = characters.joinToString("")
        if (line.isEmpty()) return

        if (currentInnerElementTag.isNotEmpty()) {
            currentInnerElementContent += line
        } else if (currentElementTag.isNotEmpty()) {
            currentElementContent += line
        } else {
            log.warn("Content without tags: $line.")
        }
    }

    override fun startElement(uri: String?, localName: String?, qName: String?, attributes: Attributes?) {
        if (attributes?.let { it.length > 0 } == true) {
            reportText += "Error: Found an attribute, but none were expected.\n"
        }
        if (localName == null) {
            reportText += "Error: Element has no local name in URI $uri.\n"
            return
        }
        if (localName.any { it in invalidPropertyNameCharacters }) {
            reportText += "Error: Element '$localName' contains invalid characters: $invalidPropertyNameCharacters.\n"
            return
        }
        if (localName == recordTag) return
        if (currentElementTag.isEmpty()) {
            currentElementTag = localName
        } else {
            currentInnerElementTag = localName
        }
    }

    override fun endElement(uri: String?, localName: String?, qName: String?) {
        // Ignore if the current tag is the record tag
        if (recordTag == localName) {
            return
        }

        // End tag of the current element
        if (currentElementTag == localName) {
            if (currentElementTag == identifierFieldName) {
                // Extract the identifier value based on the given field name from configuration.
                // This is used for the message key
                identifier = currentElementContent
            }

            if (innerElements.isEmpty()) {
                // The current element has no inner elements
                if (currentElementContent.isNotEmpty()) {
                    // If the current element content is not empty, add it to the JSON result
                    when (val existingItem = jsonResult[currentElementTag]) {
                        null -> jsonResult[currentElementTag] = currentElementContent
                        is List<*> -> jsonResult[currentElementTag] = existingItem + listOf(currentElementContent)
                        else -> jsonResult[currentElementTag] = listOf(existingItem, currentElementContent)
                    }
                }
            } else {
                // The current element has inner elements
                when (val existingItem = jsonResult[currentElementTag]) {
                    null -> jsonResult[currentElementTag] = innerElements.toMap()
                    is List<*> -> jsonResult[currentElementTag] = existingItem + listOf(innerElements.toMap())
                    else -> jsonResult[currentElementTag] = listOf(existingItem, innerElements.toMap())
                }

                innerElements.clear()
            }

            currentElementTag = ""
            currentElementContent = ""
        } else if (currentInnerElementTag == localName) {
            // End tag of an inner element
            if (currentInnerElementContent.isNotEmpty()) {
                innerElements.add(Pair(currentInnerElementTag, currentInnerElementContent))
            }

            currentInnerElementTag = ""
            currentInnerElementContent = ""
        } else {
            // Unmatched end tag
            reportText += "Unmatched end element: $localName.\n"
        }
    }

    override fun endDocument() {
        output.write(Json.encodeToString(jsonResult.toJsonElement()))
        report = if (reportText.isNotEmpty()) {
            reports.fatal(
                identifier ?: key,
                "TRANSFORMATION ISSUE: ${reportText.trim()}"
            )
        } else {
            reports.success(identifier ?: key, "Successfully transformed xml to json!")
        }
    }
}


fun Map<*, *>.toJsonElement(): JsonElement {
    val map = HashMap<String, JsonElement>()
    this.forEach {
        val key = it.key as? String ?: return@forEach
        val value = it.value ?: return@forEach
        when (value) {
            // convert containers into corresponding Json containers
            is Map<*, *> -> map[key] = (value).toJsonElement()
            is List<*> -> map[key] = value.toJsonElement()
            // convert the value to a JsonPrimitive
            else -> map[key] = JsonPrimitive(value.toString())
        }
    }
    return JsonObject(map)
}

fun List<*>.toJsonElement(): JsonElement {
    val list = ArrayList<JsonElement>()

    this.forEach {
        val value = it as? Any ?: return@forEach
        when (value) {
            is Map<*, *> -> list.add((value).toJsonElement())
            is List<*> -> list.add(value.toJsonElement())
            else -> list.add(JsonPrimitive(value.toString()))
        }
    }

    return JsonArray(list)
}