/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.PropertyName.APP_VERSION
import ch.memobase.PropertyName.CONFIG_TOPIC
import ch.memobase.PropertyName.OAI_INPUT_TOPIC
import ch.memobase.PropertyName.REPORTING_STEP_NAME
import ch.memobase.settings.SettingsLoader
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.message.ObjectMessage
import kotlin.system.exitProcess

fun loadSettings(file: String = "app.yml"): SettingsLoader {
    return SettingsLoader(
        listOf(
            CONFIG_TOPIC,
            REPORTING_STEP_NAME,
            OAI_INPUT_TOPIC,
            APP_VERSION,
        ),
        file,
        useStreamsConfig = true,
        readSftpSettings = true
    )
}

class App {
    companion object {
        private val log = LogManager.getLogger(this::class.java)
        @JvmStatic
        fun main(args: Array<String>) {
            try {
                Service(loadSettings()).run()
            } catch (ex: Exception) {
                log.error(ObjectMessage(ex))
                exitProcess(1)
            }
        }
    }
}
