/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import ch.memobase.KafkaTopology
import ch.memobase.PropertyName
import ch.memobase.loadSettings
import ch.memobase.testing.EmbeddedSftpServer
import org.apache.kafka.common.header.internals.RecordHeader
import org.apache.kafka.common.header.internals.RecordHeaders
import org.apache.kafka.common.serialization.ByteArraySerializer
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.TestRecord
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File
import java.io.FileInputStream
import java.nio.charset.Charset

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestPipelines {
    private val resourcePath = "src/test/resources/data"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    private val sftpServer = EmbeddedSftpServer(22000, "user", "password")

    init {
        sftpServer.putFile(
            "/memobase/sftp/data.xml", FileInputStream("$resourcePath/integration/sftp/data.xml")
        )
    }

    private fun headers(xmlIdentifierNameField: String): RecordHeaders {
        val headers = RecordHeaders()
        headers.add(RecordHeader("sessionId", "test-session-id".toByteArray()))
        headers.add(RecordHeader("recordSetId", "test-record-set-id".toByteArray()))
        headers.add(RecordHeader("institutionId", "test-institution-id".toByteArray()))
        headers.add(RecordHeader("isPublished", "false".toByteArray()))
        headers.add(RecordHeader("xmlRecordTag", "record".toByteArray()))
        headers.add(RecordHeader("xmlIdentifierFieldName", xmlIdentifierNameField.toByteArray()))
        headers.add(RecordHeader("tableSheetIndex", "1".toByteArray()))
        headers.add(RecordHeader("tableHeaderCount", "1".toByteArray()))
        headers.add(RecordHeader("tableHeaderIndex", "1".toByteArray()))
        headers.add(RecordHeader("tableIdentifierIndex", "1".toByteArray()))
        return headers
    }

    @Test
    fun `test sftp pipeline`() {
        val settingsLoader = loadSettings("integration.yml")
        val driver = TopologyTestDriver(KafkaTopology(settingsLoader).build(), settingsLoader.kafkaStreamsSettings)
        driver.use { testDriver ->
            val inputTopic =
                testDriver.createInputTopic(settingsLoader.inputTopic, StringSerializer(), StringSerializer())
            val configTopic = testDriver.createInputTopic(
                settingsLoader.appSettings.getProperty(PropertyName.CONFIG_TOPIC),
                ByteArraySerializer(),
                ByteArraySerializer()
            )
            val outputTopic =
                testDriver.createOutputTopic(settingsLoader.outputTopic, StringDeserializer(), StringDeserializer())
            val reportTopic = testDriver.createOutputTopic(
                settingsLoader.processReportTopic, StringDeserializer(), StringDeserializer()
            )

            configTopic.pipeInput(
                "test-record-set-id#transform".toByteArray(Charset.defaultCharset()),
                readFile("integration/sftp/transform.xslt").toByteArray(Charset.defaultCharset())
            )

            inputTopic.pipeInput(
                TestRecord(
                    "data.xml", readFile("integration/sftp/input.json"), headers("identifierMain")
                )
            )

            assertAll("sftp pipeline integration", {
                assertThat(outputTopic.queueSize).isEqualTo(1)
            }, {
                assertThat(reportTopic.queueSize).isEqualTo(1)
            })
        }
    }


    @Test
    fun `test sftp pipeline without xslt file present`() {
        val settingsLoader = loadSettings("integration.yml")
        val driver = TopologyTestDriver(KafkaTopology(settingsLoader).build(), settingsLoader.kafkaStreamsSettings)
        driver.use { testDriver ->
            val inputTopic =
                testDriver.createInputTopic(settingsLoader.inputTopic, StringSerializer(), StringSerializer())
            val outputTopic =
                testDriver.createOutputTopic(settingsLoader.outputTopic, StringDeserializer(), StringDeserializer())
            val reportTopic = testDriver.createOutputTopic(
                settingsLoader.processReportTopic, StringDeserializer(), StringDeserializer()
            )

            inputTopic.pipeInput(
                TestRecord(
                    "data.xml", readFile("integration/sftp/input.json"), headers("identifierMain")
                )
            )
            assertAll("sftp pipeline integration", {
                assertThat(outputTopic.queueSize).isEqualTo(0)
            }, {
                assertThat(reportTopic.queueSize).isEqualTo(1)
            })
        }
    }

    @Test
    fun `test oai pipeline`() {
        val settingsLoader = loadSettings("integration.yml")
        val driver = TopologyTestDriver(KafkaTopology(settingsLoader).build(), settingsLoader.kafkaStreamsSettings)
        driver.use { testDriver ->
            val inputTopic = testDriver.createInputTopic(
                settingsLoader.appSettings.getProperty(PropertyName.OAI_INPUT_TOPIC), StringSerializer(), StringSerializer()
            )
            val configTopic = testDriver.createInputTopic(
                settingsLoader.appSettings.getProperty(PropertyName.CONFIG_TOPIC),
                ByteArraySerializer(),
                ByteArraySerializer()
            )
            val outputTopic =
                testDriver.createOutputTopic(settingsLoader.outputTopic, StringDeserializer(), StringDeserializer())
            val reportTopic = testDriver.createOutputTopic(
                settingsLoader.processReportTopic, StringDeserializer(), StringDeserializer()
            )
            configTopic.pipeInput(
                "test-record-set-id#transform".toByteArray(),
                readFile("integration/oai/transform.xslt").toByteArray()
            )

            inputTopic.pipeInput(TestRecord("test-oai", readFile("integration/oai/input.xml"), headers("id")))

            assertAll("oai pipeline integration", {
                assertThat(outputTopic.queueSize).isEqualTo(1)
            }, {
                assertThat(reportTopic.queueSize).isEqualTo(1)
            })
        }
    }


    @Test
    fun `test oai pipeline without config`() {
        val settingsLoader = loadSettings("integration.yml")
        val driver = TopologyTestDriver(KafkaTopology(settingsLoader).build(), settingsLoader.kafkaStreamsSettings)
        driver.use { testDriver ->
            val inputTopic = testDriver.createInputTopic(
                settingsLoader.appSettings.getProperty(PropertyName.OAI_INPUT_TOPIC),
                StringSerializer(),
                StringSerializer()
            )
            val outputTopic =
                testDriver.createOutputTopic(settingsLoader.outputTopic, StringDeserializer(), StringDeserializer())
            val reportTopic = testDriver.createOutputTopic(
                settingsLoader.processReportTopic, StringDeserializer(), StringDeserializer()
            )
            inputTopic.pipeInput(TestRecord("test-oai", readFile("integration/oai/input.xml"), headers("id")))

            assertAll("oai pipeline integration", {
                assertThat(outputTopic.queueSize).isEqualTo(0)
            }, {
                assertThat(reportTopic.queueSize).isEqualTo(1)
            })
        }
    }


    @Test
    fun `test multi file output`() {
        val settingsLoader = loadSettings("integration.yml")
        val driver = TopologyTestDriver(KafkaTopology(settingsLoader).build(), settingsLoader.kafkaStreamsSettings)
        driver.use { testDriver ->
            val inputTopic = testDriver.createInputTopic(
                settingsLoader.appSettings.getProperty(PropertyName.OAI_INPUT_TOPIC),
                StringSerializer(),
                StringSerializer()
            )
            val configTopic = testDriver.createInputTopic(
                settingsLoader.appSettings.getProperty(PropertyName.CONFIG_TOPIC),
                ByteArraySerializer(),
                ByteArraySerializer()
            )
            val outputTopic =
                testDriver.createOutputTopic(settingsLoader.outputTopic, StringDeserializer(), StringDeserializer())
            val reportTopic = testDriver.createOutputTopic(
                settingsLoader.processReportTopic, StringDeserializer(), StringDeserializer()
            )
            configTopic.pipeInput(
                "test-record-set-id#transform".toByteArray(),
                readFile("test_multi_file_output/transform.xslt").toByteArray()
            )
            inputTopic.pipeInput(
                TestRecord(
                    "test-oai",
                    readFile("test_multi_file_output/input.xml"),
                    headers("id")
                )
            )

            assertAll("multi file output", {
                assertThat(outputTopic.queueSize).isEqualTo(5)
            }, {
                assertThat(reportTopic.queueSize).isEqualTo(5)
            })

        }
    }
}