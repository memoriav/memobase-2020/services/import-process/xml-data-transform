/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import ch.memobase.Reports
import ch.memobase.XsltHandler
import ch.memobase.reporting.ReportStatus
import ch.memobase.settings.HeaderMetadata
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File
import java.io.FileInputStream
import java.nio.charset.Charset

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestXsltTransform {
    private val resourcePath = "src/test/resources/data"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    private val reports = Reports("test", "testVersion")

    @Test
    fun `test xslt transform functionality`() {
        val meta = HeaderMetadata(
            "test", "test", "test", false, "record", "identifierMain", 0, 0, 0, 0
        )

        val transformer = XsltHandler(reports)
        val xslt = FileInputStream("${resourcePath}/test_transform/test-transformer.xslt").readBytes()
        val result = transformer.apply(
            "source-key",
            meta,
            FileInputStream("${resourcePath}/test_transform/test-transformer.xml"),
            File("${resourcePath}/test_transform/test-transformer.xml").readText(),
            xslt
        )
        val key = result.first().key
        val output = result.first().output
        val report = result.first().report
        assertAll("test xslt transformation functionality.", {
            assertThat(key).isEqualTo("ADG-102683")
        }, {
            assertThat(output).isEqualTo(readFile("test_transform/output.json"))
        }, {
            assertThat(report.status).isEqualTo(ReportStatus.success)
        }, {
            assertThat(report.message).isEqualTo("Successfully transformed xml to json!")
        }, {
            assertThat(report.content).isEqualTo(null)
        }, {
            assertThat(report.id).isEqualTo("ADG-102683")
        })
    }

    @Test
    @Disabled
    fun `parsing adm-001-15502 should be successful`() {
        val meta = HeaderMetadata(
            "adm-001", "test", "adm", false, "record", "identifierOriginal", 1, 1, 1, 1
        )

        val transformer = XsltHandler(reports)
        val xslt = FileInputStream("${resourcePath}/test_transform/15502-transform.xslt").readBytes()
        val result = transformer.apply(
            "source-key",
            meta,
            FileInputStream("${resourcePath}/test_transform/15502.xml"),
            File("${resourcePath}/test_transform/15502.xml").readText(),
            xslt
        )
        assertThat(result.first().report.status).isNotEqualTo("FATAL")
    }

    @Test
    fun `test xslt exception handling`() {
        val meta = HeaderMetadata(
            "zbz-001", "zbz-001-1", "zbz", true, "record", "id", 0, 0, 0, 0
        )

        val transformer = XsltHandler(reports)
        val xslt = FileInputStream("${resourcePath}/test_oai_xslt/transform.xslt").readBytes()
        val result = transformer.apply(
            "990107184760205508",
            meta,
            FileInputStream("${resourcePath}/test_oai_xslt/input.xml"),
            File("${resourcePath}/test_oai_xslt/input.xml").readText(),
            xslt
        )
        val key = result.first().key
        val output = result.first().output
        val report = result.first().report
        assertAll("test xslt transformation functionality.", {
            assertThat(key).isEqualTo("990107184760205508")
        }, {
            assertThat(report.status).isEqualTo(ReportStatus.fatal)
        },
        {
            assertThat(report.id).isEqualTo("990107184760205508")
        }, {
            assertThat(output).isEqualTo("")
        })
    }

    @Test
    fun `test basic xslt`() {
        val meta = HeaderMetadata(
            "zbz-001", "zbz-001-1", "zbz", true, "record", "IDBilder", 0, 0, 0, 0
        )

        val transformer = XsltHandler(reports)
        val xslt = FileInputStream("${resourcePath}/test_no_transformation/transform.xslt").readBytes()
        val result = transformer.apply(
            "322622",
            meta,
            FileInputStream("${resourcePath}/test_no_transformation/input.xml"),
            File("${resourcePath}/test_no_transformation/input.xml").readText(),
            xslt
        )
        val key = result.first().key
        val output = result.first().output
        val report = result.first().report
        assertAll("test xslt transformation functionality.", {
            assertThat(key).isEqualTo("322622")
        }, {
            assertThat(report.status).isEqualTo(ReportStatus.success)
        },
        {
            assertThat(report.id).isEqualTo("322622")
        }, {
            assertThat(output).isEqualTo(File("${resourcePath}/test_no_transformation/output.json").readText())
        })
    }
}
