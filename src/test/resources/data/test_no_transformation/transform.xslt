<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <xsl:output
            indent="yes"
            method="xml"
    />

    <xsl:template match="@*|node()">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="@*|node()"/>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>