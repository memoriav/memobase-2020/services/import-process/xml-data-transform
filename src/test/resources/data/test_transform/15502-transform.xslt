<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:ec="urn:ebu:metadata-schema:ebuCore_2012"
    xpath-default-namespace="urn:ebu:metadata-schema:ebuCore_2012"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output indent="yes" method="xml"/>
    
    <xsl:template match="@*|node()">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>
    
    <xsl:template match="ec:coreMetadata">
        <!--<xsl:variable name="id" select="ec:identifier[@typeLabel='Original']/dc:identifier"/>
        <xsl:result-document href="{$id}.xml">
            <xsl:element name="record">
                <xsl:apply-templates />
            </xsl:element>    
        </xsl:result-document>-->
        <xsl:for-each select=".">
            <xsl:element name="record">
                <xsl:apply-templates />
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
        
    <!-- content of child node is copied to parent node -->
    <xsl:template match="ec:title | ec:creator | ec:language | ec:coverage/ec:spatial/ec:location | ec:format/ec:essenceLocator">
        <xsl:element name="{local-name()}">
            <xsl:choose>
                <xsl:when test="contains(child::*, '(Autore)')">
                    <xsl:value-of select="substring-before(child::*, '(Autore)')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="child::*"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>
    
    <!-- content of typeLabel is transformed to an element name and content of child node is copied to it-->
    <xsl:template match="ec:description[@typeLabel]">
        <xsl:variable name="typeLabel" select="@typeLabel"/>
        <xsl:element name="{$typeLabel}">
            <xsl:value-of select="child::*"/>
        </xsl:element>
    </xsl:template>
    
    <!-- content of typeLabel is copied as content of the node -->
    <xsl:template match="ec:format/ec:medium">
        <xsl:element name="{local-name()}">
            <xsl:value-of select="@typeLabel"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="ec:type/ec:objectType">
        <xsl:element name="objectType">
            <xsl:choose>
                <xsl:when test="matches(@typeLabel, 'Image', 'i')">
                    <xsl:text>Foto</xsl:text>
                </xsl:when>
            </xsl:choose>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="ec:identifier">
        <xsl:variable name="typeLabel" select="@typeLabel"/>
        <xsl:element name="identifier{$typeLabel}">
            <!--<xsl:value-of select="child::*"/>-->
            <xsl:value-of select="normalize-space(dc:identifier)"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="ec:rights[@typeLabel = 'Access']/dc:rights">
        <xsl:if test="matches(., 'sul posto')">
            <xsl:element name="accessPhysical">
                <xsl:text>onsite</xsl:text>
            </xsl:element>
            <xsl:element name="accessDigital">
                <xsl:text>public</xsl:text>
            </xsl:element>
            <xsl:element name="rightsStatementText">
                <xsl:text>Copyright Not Evaluated (CNE)</xsl:text>
            </xsl:element>
            <xsl:element name="rightsStatementURL">
                <xsl:text>http://rightsstatements.org/vocab/CNE/1.0/</xsl:text>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="ec:rights[@typeLabel='Holder']/ec:rightsHolder/ec:contactDetails/ec:name">
        <xsl:element name="rightsHolder">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="ec:rights[@typeLabel='Usage']/dc:rights">
        <xsl:element name="rightsUsage">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="ec:date/ec:created | ec:coverage/ec:temporal/ec:PeriodOfTime">
        <xsl:variable name="date" select="local-name()"/>
        <xsl:for-each select=".">
            <xsl:element name="{$date}Date">
                <xsl:choose>
                    <xsl:when test="descendant::periodName">
                        <xsl:value-of select="descendant::periodName"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="dates"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="dates">
        <xsl:variable name="startDate" select="if (ends-with(@startDate, '0000')) then (substring-before(@startDate, '0000')) else (@startDate[. != ''])"/>
        <xsl:variable name="startYear" select="@startYear[. != '']"/>
        <xsl:variable name="startTime" select="@startTime[. != '']"/>
        <xsl:variable name="endDate" select="if (ends-with(@endDate, '0000')) then (substring-before(@endDate, '0000')) else (@endDate[. != ''])"/>
        <xsl:variable name="endYear" select="@endYear[. != '']"/>
        <xsl:variable name="period" select="@period[. != '']"/>
        
        <xsl:choose>
            <!-- Combinations which are not present in created/issued/temporal and are not processed:
                     * endYear and startDate
                     * period and endDate and startYear
                     * period and endYear and startDate
                     * startTime in another combination but with startDate
                     * period and endDate only
                -->
            <xsl:when test="$startDate and not($endDate | $endYear | $period | $startTime)">
                <xsl:value-of select="$startDate"/>
            </xsl:when>
            <xsl:when test="$startYear and not($endDate | $endYear | $period | $startTime)">
                <xsl:value-of select="$startYear"/>
            </xsl:when>
            <xsl:when test="$startDate and $endDate and not($period | $startTime)">
                <xsl:value-of select="concat($startDate, '/', $endDate)"/>
            </xsl:when>
            <xsl:when test="$startYear and $endDate and not($period | $startTime)">
                <xsl:value-of select="concat($startYear, '/', $endDate)"/>
            </xsl:when>
            <xsl:when test="$startYear and $endYear and not($period | $startTime)">
                <xsl:value-of select="concat($startYear, '/', $endYear)"/>
            </xsl:when>
            <xsl:when test="$startTime">
                <xsl:value-of select="concat($startDate, 'T', $startTime)"/>
            </xsl:when>
            <xsl:when test="$endDate and not ($startDate | $startYear | $period)">
                <xsl:value-of select="concat('?/', $endDate)"/>
            </xsl:when>
            <xsl:when test="$endYear and not ($startDate | $startYear | $period)">
                <xsl:value-of select="concat('?/', $endYear)"/>
            </xsl:when>
            <xsl:when test="$period and not($endDate | $endYear | $startDate | $startYear)">
                <xsl:value-of select="$period"/>
            </xsl:when>
            <xsl:when test="$period and $startDate and not ($endDate | $endYear)">
                <xsl:value-of select="concat($period, ' ', $startDate)"/>
            </xsl:when>
            <xsl:when test="$period and $startYear and not ($endDate | $endYear)">
                <xsl:value-of select="concat($period, ' ', $startYear)"/>
            </xsl:when>
            <xsl:when test="$period and $startDate and $endDate">
                <!--<xsl:value-of select="concat($period, ' ', year-from-date(xs:date(replace($startDate,'(\d{4})(\d{2})(\d{2})','$1-$2-$3'))), $startDate, '/', $endDate)"/>-->
                <xsl:value-of select="concat($period, ' ', $startDate, '/', $endDate)"/>
            </xsl:when>
            <xsl:when test="$period and $startYear and $endYear">
                <xsl:choose>
                    <xsl:when test="matches(@period,concat(@startYear, '-', @endYear))">
                        <xsl:value-of select="concat($startYear, '/', $endYear)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat($period, ' ', $startYear, '/', $endYear)"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="ec:format">
        <xsl:variable name="format" select="local-name()"/>
        <xsl:for-each select="ec:technicalAttributeString">
            <xsl:variable name="typeLabel" select="@typeLabel"/>
            <xsl:element name="{$format}{$typeLabel}">
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:for-each>
        <xsl:for-each select="ec:width | ec:height">
            <xsl:variable name="local" select="local-name()"/>
            <xsl:variable name="unit" select="@unit"/>
            <xsl:element name="{$local}">
                <xsl:value-of select="concat(., ' ', $unit)"/>
            </xsl:element>
        </xsl:for-each>
        <xsl:apply-templates/>
    </xsl:template>
    
</xsl:stylesheet>