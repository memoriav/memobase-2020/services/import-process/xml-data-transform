<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns="http://www.loc.gov/MARC21/slim"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd"
                xpath-default-namespace="http://www.loc.gov/MARC21/slim"
                version="2.0">

    <xsl:output
            indent="yes"
            method="xml"
    />

    <xsl:key name="lookup-role" match="role" use="@code"/>

    <xsl:template match="@*|node()">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>

    <xsl:template match="/">
        <xsl:element name="record"><xsl:apply-templates/></xsl:element>
    </xsl:template>

    <xsl:template match="controlfield[@tag='001']">
        <xsl:element name="id">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="controlfield[@tag='008']">
        <xsl:choose>
            <xsl:when test="exists(following-sibling::datafield[@tag='041']/subfield[@code='a'])">
                <xsl:for-each select="following-sibling::datafield[@tag='041']/subfield[@code='a']/text()">
                    <xsl:element name="language">
                        <xsl:choose>
                            <xsl:when test="matches(., '\|\|\|')" />
                            <xsl:when test="string-length(.) &gt; 3" />
                            <xsl:otherwise>
                                <xsl:value-of select="." />
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:element>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="language">
                    <xsl:choose>
                        <xsl:when test="matches(substring(.,36,3), '\|\|\|')" />
                        <xsl:otherwise>
                            <xsl:value-of select="substring(.,36,3)"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Für alle Daten aus SLSP nutzbar, generiert Backlink auf swisscovery.slsp.ch (Network Zone von SLSP) -->
    <xsl:template match="datafield[@tag='035']/subfield[@code='a']">
        <xsl:if test="starts-with(., '(EXLNZ-41SLSP_NETWORK)')">
            <xsl:element name="swisscoveryURL">
                <xsl:value-of select="concat('https://swisscovery.slsp.ch/permalink/41SLSP_NETWORK/1ufb5t2/alma', substring-after(., '(EXLNZ-41SLSP_NETWORK)'))"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="starts-with(., '(NEBIS)')">
            <xsl:element name="nebisID">
                <xsl:value-of select="substring-before(substring-after(., '(NEBIS)'), 'EBI01')"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template match="datafield[@tag='240']">
        <xsl:element name="titleUniform">
            <xsl:text>Einheitstitel: </xsl:text>
            <xsl:call-template name="titlePortion_uniform"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='245']">
        <xsl:element name="titleMain">
            <xsl:call-template name="titlePortion_subtitle"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='245']/subfield[@code='c']">
        <xsl:element name="statementOfResponsibility">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='246']">
        <xsl:element name="titleAlternative">
            <xsl:choose>
                <xsl:when test="child::subfield[@code='i']">
                    <xsl:value-of select="concat(child::subfield[@code='i'], ': ')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of><xsl:text>Alternativtitel: </xsl:text></xsl:value-of>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:call-template name="titlePortion_subtitle"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='264']/subfield[@code='b']">
        <xsl:element name="publisher">
            <xsl:if test=". != '-' and
                         . !='[S.l.]' and
                         . !='[s.l.]' and
                         . !='[S. l.]' and
                         . !='[s. l.]' and
                         . !='s. l.' and
                         . !='[S.l]' and
                         . !='[...]' and
                         . !='...' and
                         . !='[Var.loc.]' and
                         . !='[var.loc.]' and
                         . !='[Var.loc]' and
                         . !='[var.loc]' and
                         . !='[Var.loc. ]' and
                         . !='[var.loc. ]' and
                         . !='[O.O.]' and
                         . !='[o.O.]' and
                         . !='O.O.' and
                         . !='o.O.' and
                         . !='Div.' and
                         . !='div.' and
                         . !='[Div.]' and
                         . !='[div.]' and
                         . !='[Div. Erscheinungsorte]' and
                         . !='[Ohne Ort]' and
                         . !='Versch. Orte' and
                         . !='[Versch. Orte]' and
                         . !='[Erscheinungsort nicht ermittelbar]' and
                         . !='[Absendeort nicht ermittelbar]' and
                         . !='[Entstehungsort nicht ermittelbar]' and
                         . !='[Herstellungsort nicht ermittelbar]' and
                         . !='[Lieu de publication non identifié]'">
                <xsl:value-of select="replace(replace(.,'ß', 'ss', 'i'), '\[|\]|\?| etc.| \[etc.\]', '', 'i')"/>
            </xsl:if>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='264' and matches(@ind2, '0')]/subfield[@code='c']">
        <xsl:element name="dateCreated">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='264' and matches(@ind2, '[1-3]')]/subfield[@code='c']">
        <xsl:element name="dateIssued">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='300']">
        <xsl:element name="physicalDescription">
            <xsl:if test="child::subfield[@code='a'] and child::subfield[@code='a']/text() != ''">
                <xsl:value-of select="child::subfield[@code='a'][1]"/>
            </xsl:if>
            <xsl:if test="child::subfield[@code='b'] and child::subfield[@code='b']/text() != ''">
                <xsl:value-of select="concat(' : ', child::subfield[@code='b'][1])"/>
            </xsl:if>
            <xsl:if test="child::subfield[@code='c'] and child::subfield[@code='c']/text() != ''">
                <xsl:value-of select="concat(' ; ', child::subfield[@code='c'][1])"/>
            </xsl:if>
            <xsl:if test="child::subfield[@code='e'] and child::subfield[@code='e']/text() != ''">
                <xsl:value-of select="concat(' + ', child::subfield[@code='e'][1])"/>
            </xsl:if>
            <xsl:if test="child::subfield[@code='3'] and child::subfield[@code='3']/text() != ''">
                <xsl:value-of select="concat(' (', child::subfield[@code='3'][1]), ')'"/>
            </xsl:if>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='382']">
        <xsl:for-each select="child::subfield[@code='a']">
            <xsl:element name="subjectTopic">
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="datafield[@tag='500']/subfield[@code='a']">
        <xsl:element name="note">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='506']/subfield[@code='a']">
        <xsl:element name="termsGoverningAccess">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='510']/subfield[@code='a']">
        <xsl:element name="references">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='520']/subfield[@code='a']">
        <xsl:element name="abstract">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='540']/subfield[@code='a']">
        <xsl:element name="termsGoverningUse">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='542']/subfield[@code='d']">
        <xsl:element name="copyrightHolder">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="datafield[@tag='544']/subfield[matches(@code, 'a|n')]">
        <xsl:element name="otherArchivalMaterial">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='561']/subfield[@code='a']">
        <xsl:element name="ownershipHistory">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='581']/subfield[@code='a']">
        <xsl:element name="publications">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='600']">
        <xsl:element name="subjectPerson">
            <xsl:call-template name="person"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="datafield[matches(@tag, '610')]">
        <xsl:element name="subjectCorporateBody">
            <xsl:call-template name="corporateBody"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[matches(@tag, '611')]">
        <xsl:element name="subjectCorporateBody">
            <xsl:call-template name="conference"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='650']/subfield[@code='a']">
        <xsl:element name="subjectTopic">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='651']">
        <xsl:element name="subjectPlace">
            <xsl:element name="name">
                <xsl:call-template name="place_name"/>
            </xsl:element>
            <xsl:if test="exists(child::subfield[@code='0'])">
                <xsl:element name="identifier">
                    <xsl:call-template name="identifier"/>
                </xsl:element>
            </xsl:if>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="datafield[@tag='655']">
        <xsl:choose>
            <xsl:when test="matches(subfield[@code='2'], 'gnd-content|idsmusg')">
                <xsl:element name="genre">
                    <xsl:value-of select="child::subfield[@code='a']"/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="matches(subfield[@code='2'], 'rero')">
                <xsl:element name="genre">
                    <xsl:value-of select="replace(child::subfield[@code='a'], '[|]', '')"/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="matches(subfield[@code='2'], 'gnd-carrier')">
                <xsl:element name="carrier">
                    <xsl:value-of select="child::subfield[@code='a']"/>
                </xsl:element>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="datafield[@tag='773']">
        <xsl:element name="containedIn">
            <xsl:if test="child::subfield[@code='t'] and child::subfield[@code='t']/text() != ''">
                <xsl:value-of select="concat('Enthalten in: ', child::subfield[@code='t'][1])"/>
            </xsl:if>
            <xsl:if test="child::subfield[@code='d'] and child::subfield[@code='d']/text() != ''">
                <xsl:value-of select="concat('. ', child::subfield[@code='d'][1])"/>
            </xsl:if>
            <xsl:if test="child::subfield[@code='g'] and child::subfield[@code='g']/text() != ''">
                <xsl:value-of select="concat('. ', child::subfield[@code='g'][1])"/>
            </xsl:if>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[matches(@tag, '100')]">
        <xsl:element name="creatorPerson">
            <xsl:call-template name="person"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[matches(@tag, '110')]">
        <xsl:element name="creatorCorporateBody">
            <xsl:call-template name="corporateBody"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[matches(@tag, '111')]">
        <xsl:element name="creatorCorporateBody">
            <xsl:call-template name="conference"/>
        </xsl:element>
    </xsl:template>


    <xsl:template match="datafield[matches(@tag, '700')]">
        <xsl:choose>
            <!-- All MARC relator codes which are used for creators according to RDA are mapped to creator. Remove the code if it should be mapped to contributor -->
            <xsl:when test="matches(child::subfield[@code='4'], 'arc|art|aus|aut|chr|cll|cmp|com|cre|ctg|dsr|fmk|inv|isb|ive|ivr|lbt|lyr|pht|pra|rsp|scl')">
                <xsl:element name="creatorPerson">
                    <xsl:call-template name="person"/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="matches(child::subfield[@code='4'], 'fmp|pro|rpc|tlp')">
                <xsl:element name="producerPerson">
                    <xsl:call-template name="person"/>
                </xsl:element>
            </xsl:when>
            <!-- ignore fields with subfield t, contains an entry for a work, not for a person -->
            <xsl:when test="exists(child::subfield[@code='t'])"/>
            <xsl:otherwise>
                <xsl:element name="contributorPerson">
                    <xsl:call-template name="person"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="datafield[matches(@tag, '710')]">
        <xsl:choose>
            <!-- All MARC relator codes which are used for creators according to RDA are mapped to creator. Remove the code if it should be mapped to contributor -->
            <xsl:when test="matches(child::subfield[@code='4'], 'arc|art|aus|aut|chr|cll|cmp|com|cre|ctg|dsr|fmk|inv|isb|ive|ivr|lbt|lyr|pht|pra|rsp|scl')">
                <xsl:element name="creatorCorporateBody">
                    <xsl:call-template name="corporateBody"/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="matches(child::subfield[@code='4'], 'fmp|pro|rpc|tlp')">
                <xsl:element name="producerCorporateBody">
                    <xsl:call-template name="corporateBody"/>
                </xsl:element>
            </xsl:when>
            <!-- ignore fields with subfield t, contains an entry for a work, not for a person -->
            <xsl:when test="exists(child::subfield[@code='t'])"/>
            <xsl:otherwise>
                <xsl:element name="contributorCorporateBody">
                    <xsl:call-template name="corporateBody"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="datafield[matches(@tag, '711')]">
        <xsl:choose>
            <!-- All MARC relator codes which are used for creators according to RDA are mapped to creator. Remove the code if it should be mapped to contributor -->
            <xsl:when test="matches(child::subfield[@code='4'], 'arc|art|aus|aut|chr|cll|cmp|com|cre|ctg|dsr|fmk|inv|isb|ive|ivr|lbt|lyr|pht|pra|rsp|scl')">
                <xsl:element name="creatorConference">
                    <xsl:call-template name="conference"/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="matches(child::subfield[@code='4'], 'fmp|pro|rpc|tlp')">
                <xsl:element name="producerConference">
                    <xsl:call-template name="conference"/>
                </xsl:element>
            </xsl:when>
            <!-- ignore fields with subfield t, contains an entry for a work, not for a person -->
            <xsl:when test="exists(child::subfield[@code='t'])"/>
            <xsl:otherwise>
                <xsl:element name="contributorConference">
                    <xsl:call-template name="conference"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="datafield[@tag='852']/subfield[@code='j']">
        <xsl:if test="starts-with(., 'Mus NL 147')">
            <xsl:element name="callNumber">
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:if>
    </xsl:template>




    <!-- use for titles with subtitle -->
    <xsl:template name="titlePortion_subtitle">
        <xsl:value-of select="replace(child::subfield[@code='a'],'&lt;|&gt;', '')"/>
        <xsl:if test="child::subfield[@code='b'] and child::subfield[@code='b']/text() != ''">
            <xsl:value-of select="concat(' : ', child::subfield[@code='b'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='n'] and child::subfield[@code='n']/text() != ''">
            <xsl:for-each select="child::subfield[@code='n']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='p'] and child::subfield[@code='p']/text() != ''">
            <xsl:for-each select="child::subfield[@code='p']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <!-- use for uniform title entries where title is in $a -->
    <xsl:template name="titlePortion_uniform">
        <xsl:value-of select="replace(child::subfield[@code='a'],'&lt;|&gt;', '')"/>
        <xsl:if test="child::subfield[@code='t'] and child::subfield[@code='t']/text() != ''">
            <xsl:value-of select="concat('. ', child::subfield[@code='t'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='g'] and child::subfield[@code='g']/text() != ''">
            <xsl:for-each select="child::subfield[@code='g']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='m'] and child::subfield[@code='m']/text() != ''">
            <xsl:for-each select="child::subfield[@code='m']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='n'] and child::subfield[@code='n']/text() != ''">
            <xsl:for-each select="child::subfield[@code='n']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='r'] and child::subfield[@code='r']/text() != ''">
            <xsl:value-of select="concat(', ', child::subfield[@code='r'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='f'] and child::subfield[@code='f']/text() != ''">
            <xsl:value-of select="concat('. ', child::subfield[@code='f'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='s'] and child::subfield[@code='s']/text() != ''">
            <xsl:for-each select="child::subfield[@code='s']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='p'] and child::subfield[@code='p']/text() != ''">
            <xsl:for-each select="child::subfield[@code='p']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='k'] and child::subfield[@code='k']/text() != ''">
            <xsl:for-each select="child::subfield[@code='k']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='o'] and child::subfield[@code='o']/text() != ''">
            <xsl:value-of select="concat(' ; ', child::subfield[@code='o'][1])"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="place_name">
        <xsl:value-of select="child::subfield[@code='a'][1]"/>
        <xsl:if test="child::subfield[@code='g']/text()">
            <xsl:value-of select="concat(', ', child::subfield[@code='g'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='x']/text()">
            <xsl:value-of select="concat(' (', child::subfield[@code='x'][1], ')')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="person">
        <xsl:element name="name">
            <xsl:call-template name="person_name"/>
        </xsl:element>
        <xsl:if test="exists(child::subfield[@code='0'])">
            <xsl:element name="identifier">
                <xsl:call-template name="identifier"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="exists(child::subfield[@code='d'])">
            <xsl:call-template name="person_date"/>
        </xsl:if>
        <xsl:if test="exists(child::subfield[@code='4'])">
            <xsl:call-template name="role"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="corporateBody">
        <xsl:element name="name">
            <xsl:call-template name="corporate_name"/>
        </xsl:element>
        <xsl:if test="exists(child::subfield[@code='0'])">
            <xsl:element name="identifier">
                <xsl:call-template name="identifier"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="exists(child::subfield[@code='4'])">
            <xsl:call-template name="role"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="conference">
        <xsl:element name="name">
            <xsl:call-template name="conference_name"/>
        </xsl:element>
        <xsl:if test="exists(child::subfield[@code='0'])">
            <xsl:element name="identifier">
                <xsl:call-template name="identifier"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="exists(child::subfield[@code='4'])">
            <xsl:call-template name="role"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="person_name">
        <xsl:value-of select="child::subfield[@code='a']"/>
        <xsl:if test="child::subfield[@code='b'] and child::subfield[@code='b']/text() != ''">
            <xsl:value-of select="concat(' ', replace(child::subfield[@code='b'][1], '[,.]', ''), '.')"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='c'] and child::subfield[@code='c']/text() != ''">
            <xsl:value-of select="concat(', ', child::subfield[@code='c'][1])"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="corporate_name">
        <xsl:value-of select="child::subfield[@code='a']"/>
        <xsl:if test="exists(child::subfield[@code='b']/text())">
            <xsl:for-each select="child::subfield[@code='b']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="exists(child::subfield[@code='g']/text())">
            <xsl:value-of select="concat(' (', child::subfield[@code='g'][1], ')')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="conference_name">
        <xsl:value-of select="child::subfield[@code='a']"/>
        <xsl:if test="exists(child::subfield[@code='n']/text())">
            <xsl:for-each select="child::subfield[@code='n']">
                <xsl:value-of select="concat(' ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="exists(child::subfield[@code='c']/text())">
            <xsl:for-each select="child::subfield[@code='c']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="exists(child::subfield[@code='e']/text())">
            <xsl:for-each select="child::subfield[@code='e']">
                <xsl:value-of select="concat(' ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="exists(child::subfield[@code='g']/text())">
            <xsl:for-each select="child::subfield[@code='g']">
                <xsl:value-of select="concat(' ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="exists(child::subfield[@code='d']/text())">
            <xsl:for-each select="child::subfield[@code='d']">
                <xsl:value-of select="concat(' (', ., ')')"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="identifier">
        <xsl:choose>
            <xsl:when test="matches(child::subfield[@code='0'], '^\(DE-588')">
                <xsl:value-of select="concat('https://d-nb.info/gnd/', replace(child::subfield[@code='0'], '\(DE-588\)', ''))"/>
            </xsl:when>
            <xsl:when test="matches(child::subfield[@code='0'], '^\(IDREF')">
                <xsl:value-of select="concat('http://www.idref.fr/', replace(child::subfield[@code='0'], '\(IDREF\)', ''), '/id')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="child::subfield[@code='0'][1]"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="person_date">
        <xsl:choose>
            <xsl:when test="string-length(child::subfield[@code='d']) = 9">
                <xsl:element name="birthDate">
                    <xsl:value-of select="substring-before(child::subfield[@code='d'], '-')"/>
                </xsl:element>
                <xsl:element name="deathDate">
                    <xsl:value-of select="substring-after(child::subfield[@code='d'], '-')"/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="string-length(child::subfield[@code='d']) = 5 and ends-with(child::subfield[@code='d'], '-')">
                <xsl:element name="birthDate">
                    <xsl:value-of select="substring-before(child::subfield[@code='d'], '-')"/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="string-length(child::subfield[@code='d']) = 5 and starts-with(child::subfield[@code='d'], '-')">
                <xsl:element name="deathDate">
                    <xsl:value-of select="substring-after(child::subfield[@code='d'], '-')"/>
                </xsl:element>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="role">
        <xsl:element name="role">
            <xsl:variable name="roleCode" select="child::subfield[@code='4'][1]"/>
            <xsl:variable name="roleTerm" select="$role-map/key('lookup-role', $roleCode)"/>
            <!-- change language of role here -->
            <xsl:value-of select="$roleTerm/de"/>
        </xsl:element>
    </xsl:template>

    <!-- mapping from MARC Relator Codes https://www.loc.gov/marc/relators/ to role terms, other languages can be added as needed-->
    <xsl:variable name="role-map">
        <role code="abr"><de>KürzendeR</de><en>Abridger</en></role>
        <role code="acp"><de>HerstellerIn von Nachbildungen</de><en>Art copyist</en></role>
        <role code="act"><de>SchauspielerIn</de><en>Actor</en></role>
        <role code="adi"><de>Art Director</de><en>Art director</en></role>
        <role code="adp"><de>BearbeiterIn</de><en>Adapter</en></role>
        <role code="aft"><de>VerfasserIn eines Nachworts</de><en>Author of afterword, colophon, etc.</en></role>
        <role code="anl"><de>AnalytikerIn</de><en>Analyst</en></role>
        <role code="anm"><de>TrickfilmzeichnerIn</de><en>Animator</en></role>
        <role code="ann"><de>AnnotatorIn</de><en>Annotator</en></role>
        <role code="ant"><de>BibliographischeR VorgängerIn</de><en>Bibliographic antecedent</en></role>
        <role code="ape"><de>BerufungsbeklagteR/RevisionsbeklagteR</de><en>Appellee</en></role>
        <role code="apl"><de>BerufungsklägerIn/RevisionsklägerIn</de><en>Appellant</en></role>
        <role code="app"><de>AntragstellerIn</de><en>Applicant</en></role>
        <role code="aqt"><de>AutorIn von Zitaten oder Textabschnitten</de><en>Author in quotations or text abstracts</en></role>
        <role code="arc"><de>ArchitektIn</de><en>Architect</en></role>
        <role code="ard"><de>künstlerische Leitung</de><en>Artistic director</en></role>
        <role code="arr"><de>ArrangeurIn</de><en>Arranger</en></role>
        <role code="art"><de>KünstlerIn</de><en>Artist</en></role>
        <role code="asg"><de>RechtsnachfolgerIn</de><en>Assignee</en></role>
        <role code="asn"><de>zugehöriger Name</de><en>Associated name</en></role>
        <role code="ato"><de>UnterzeichnerIn</de><en>Autographer</en></role>
        <role code="att"><de>zugehöriger Name</de><en>Attributed name</en></role>
        <role code="auc"><de>AuktionatorIn</de><en>Auctioneer</en></role>
        <role code="aud"><de>AutorIn des Dialogs</de><en>Author of dialog</en></role>
        <role code="aui"><de>VerfasserIn eines Geleitwortes</de><en>Author of introduction, etc.</en></role>
        <role code="aus"><de>DrehbuchautorIn</de><en>Screenwriter</en></role>
        <role code="aut"><de>VerfasserIn</de><en>Author</en></role>
        <role code="bdd"><de>BindungsgestalterIn</de><en>Binding designer</en></role>
        <role code="bjd"><de>EinbandgestalterIn</de><en>Bookjacket designer</en></role>
        <role code="bkd"><de>BuchgestalterIn</de><en>Book designer</en></role>
        <role code="bkp"><de>BuchherstellerIn</de><en>Book producer</en></role>
        <role code="blw"><de>AutorIn des Klappentextes</de><en>Blurb writer</en></role>
        <role code="bnd"><de>BuchbinderIn / Buchbinderei</de><en>Binder</en></role>
        <role code="bpd"><de>GestalterIn des Exlibris</de><en>Bookplate designer</en></role>
        <role code="brd"><de>Sender</de><en>Broadcaster</en></role>
        <role code="brl"><de>BrailleschriftprägerIn</de><en>Braille embosser</en></role>
        <role code="bsl"><de>BuchhändlerIn</de><en>Bookseller</en></role>
        <role code="cas"><de>FormgiesserIn</de><en>Caster</en></role>
        <role code="ccp"><de>konzeptionelle Leitung</de><en>Conceptor</en></role>
        <role code="chr"><de>ChoreografIn</de><en>Choreographer</en></role>
        <role code="cli"><de>KlientIn</de><en>Client</en></role>
        <role code="cll"><de>KalligrafIn</de><en>Calligrapher</en></role>
        <role code="clr"><de>KoloristIn</de><en>Colorist</en></role>
        <role code="clt"><de>LichtdruckerIn</de><en>Collotyper</en></role>
        <role code="cmm"><de>KommentatorIn</de><en>Commentator</en></role>
        <role code="cmp"><de>KomponistIn</de><en>Composer</en></role>
        <role code="cmt"><de>SchriftsetzerIn</de><en>Compositor</en></role>
        <role code="cnd"><de>DirigentIn</de><en>Conductor</en></role>
        <role code="cng"><de>Kameramann/frau</de><en>Cinematographer</en></role>
        <role code="cns"><de>ZensorIn</de><en>Censor</en></role>
        <role code="coe"><de>BerufungsbeklagteR im streitigen Verfahren</de><en>Contestant-appellee</en></role>
        <role code="col"><de>SammlerIn</de><en>Collector</en></role>
        <role code="com"><de>AktenbildnerIn</de><en>Compiler</en></role>
        <role code="con"><de>KonservatorIn</de><en>Conservator</en></role>
        <role code="cor"><de>AktenbildnerIn</de><en>Collection registrar</en></role>
        <role code="cos"><de>AnfechtendeR</de><en>Contestant</en></role>
        <role code="cot"><de>BerufungsklägerIn im streitigen Verfahren</de><en>Contestant-appellant</en></role>
        <role code="cou"><de>zuständiges Gericht</de><en>Court governed</en></role>
        <role code="cov"><de>UmschlaggestalterIn</de><en>Cover designer</en></role>
        <role code="cpc"><de>BeansprucherIn des Urheberrechts</de><en>Copyright claimant</en></role>
        <role code="cpe"><de>BeschwerdeführerIn-BerufungsbeklagteR</de><en>Complainant-appellee</en></role>
        <role code="cph"><de>InhaberIn des Urheberrechts</de><en>Copyright holder</en></role>
        <role code="cpl"><de>BeschwerdeführerIn/KlägerIn</de><en>Complainant</en></role>
        <role code="cpt"><de>KlägerIn/BerufungsklägerIn</de><en>Complainant-appellant</en></role>
        <role code="cre"><de>GeistigeR SchöpferIn / AktenbildnerIn</de><en>Creator</en></role>
        <role code="crp"><de>KorrespondentIn</de><en>Correspondent</en></role>
        <role code="crr"><de>KorrektorIn</de><en>Corrector</en></role>
        <role code="crt"><de>GerichtsstenografIn</de><en>Court reporter</en></role>
        <role code="csl"><de>BeraterIn</de><en>Consultant</en></role>
        <role code="csp"><de>ProjektberaterIn</de><en>Consultant to a project</en></role>
        <role code="cst"><de>KostümbildnerIn</de><en>Costume designer</en></role>
        <role code="ctb"><de>MitwirkendeR</de><en>Contributor</en></role>
        <role code="cte"><de>AnfechtungsgegnerIn-BerufungsbeklagteR</de><en>Contestee-appellee</en></role>
        <role code="ctg"><de>KartografIn</de><en>Cartographer</en></role>
        <role code="ctr"><de>VertragspartnerIn</de><en>Contractor</en></role>
        <role code="cts"><de>AnfechtungsgegnerIn</de><en>Contestee</en></role>
        <role code="ctt"><de>AnfechtungsgegnerIn-BerufungsklägerIn</de><en>Contestee-appellant</en></role>
        <role code="cur"><de>KuratorIn</de><en>Curator</en></role>
        <role code="cwt"><de>KommentatorIn</de><en>Commentator for written text</en></role>
        <role code="dbp"><de>Vertriebsort</de><en>Distribution place</en></role>
        <role code="dfd"><de>AngeklagteR/BeklagteR</de><en>Defendant</en></role>
        <role code="dfe"><de>AngeklagteR/BeklagteR-BerufungsbeklagteR</de><en>Defendant-appellee</en></role>
        <role code="dft"><de>AngeklagteR/BeklagteR-BerufungsklägerIn</de><en>Defendant-appellant</en></role>
        <role code="dgg"><de>Grad-verleihende Institution</de><en>Degree granting institution</en></role>
        <role code="dgs"><de>AkademischeR BetreuerIn</de><en>Degree supervisor</en></role>
        <role code="dis"><de>PromovierendeR</de><en>Dissertant</en></role>
        <role code="dln"><de>VorzeichnerIn</de><en>Delineator</en></role>
        <role code="dnc"><de>TänzerIn</de><en>Dancer</en></role>
        <role code="dnr"><de>DonatorIn</de><en>Donor</en></role>
        <role code="dpc"><de>AbgebildeteR</de><en>Depicted</en></role>
        <role code="dpt"><de>LeihgeberIn</de><en>Depositor</en></role>
        <role code="drm"><de>TechnischeR ZeichnerIn</de><en>Draftsman</en></role>
        <role code="drt"><de>RegisseurIn</de><en>Director</en></role>
        <role code="dsr"><de>DesignerIn</de><en>Designer</en></role>
        <role code="dst"><de>Vertrieb</de><en>Distributor</en></role>
        <role code="dtc"><de>BereitstellerIn von Daten</de><en>Data contributor</en></role>
        <role code="dte"><de>WidmungsempfängerIn</de><en>Dedicatee</en></role>
        <role code="dtm"><de>DatenmanagerIn</de><en>Data manager</en></role>
        <role code="dto"><de>WidmungsverfasserIn</de><en>Dedicator</en></role>
        <role code="dub"><de>angeblicheR AutorIn</de><en>Dubious author</en></role>
        <role code="edc"><de>BearbeiterIn der Zusammenstellung</de><en>Editor of compilation</en></role>
        <role code="edm"><de>CutterIn</de><en>Editor of moving image work</en></role>
        <role code="edt"><de>HerausgeberIn</de><en>Editor</en></role>
        <role code="egr"><de>StecherIn</de><en>Engraver</en></role>
        <role code="elg"><de>ElektrikerIn</de><en>Electrician</en></role>
        <role code="elt"><de>GalvanisiererIn</de><en>Electrotyper</en></role>
        <role code="eng"><de>IngenieurIn</de><en>Engineer</en></role>
        <role code="enj"><de>Normerlassende Gebietskörperschaft</de><en>Enacting jurisdiction</en></role>
        <role code="etr"><de>RadiererIn</de><en>Etcher</en></role>
        <role code="evp"><de>Veranstaltungsort</de><en>Event place</en></role>
        <role code="exp"><de>ExperteIn</de><en>Expert</en></role>
        <role code="fac"><de>SchreiberIn / Scriptorium</de><en>Facsimilist</en></role>
        <role code="fds"><de>Filmvertrieb</de><en>Film distributor</en></role>
        <role code="fld"><de>BereichsleiterIn</de><en>Field director</en></role>
        <role code="flm"><de>BearbeiterIn des Films</de><en>Film editor</en></role>
        <role code="fmd"><de>FilmregisseurIn</de><en>Film director</en></role>
        <role code="fmk"><de>FilmemacherIn</de><en>Filmmaker</en></role>
        <role code="fmo"><de>VorbesitzerIn</de><en>Former owner</en></role>
        <role code="fmp"><de>FilmproduzentIn</de><en>Film producer</en></role>
        <role code="fnd"><de>GründerIn</de><en>Funder</en></role>
        <role code="fpy"><de>Erste Partei</de><en>First party</en></role>
        <role code="frg"><de>FälscherIn</de><en>Forger</en></role>
        <role code="gis"><de>GeographIn</de><en>Geographic information specialist</en></role>
        <role code="his"><de>Gastgebende Institution</de><en>Host institution</en></role>
        <role code="hnr"><de>GefeierteR</de><en>Honoree</en></role>
        <role code="hst"><de>GastgeberIn</de><en>Host</en></role>
        <role code="ill"><de>IllustratorIn</de><en>Illustrator</en></role>
        <role code="ilu"><de>IlluminatorIn</de><en>Illuminator</en></role>
        <role code="ins"><de>Beschriftende Person</de><en>Inscriber</en></role>
        <role code="inv"><de>ErfinderIn</de><en>Inventor</en></role>
        <role code="isb"><de>Herausgebendes Organ</de><en>Issuing body</en></role>
        <role code="itr"><de>InstrumentalmusikerIn</de><en>Instrumentalist</en></role>
        <role code="ive"><de>InterviewteR</de><en>Interviewee</en></role>
        <role code="ivr"><de>InterviewerIn</de><en>Interviewer</en></role>
        <role code="jud"><de>RichterIn</de><en>Judge</en></role>
        <role code="jug"><de>zuständige Gerichtsbarkeit</de><en>Jurisdiction governed</en></role>
        <role code="lbr"><de>Labor</de><en>Laboratory</en></role>
        <role code="lbt"><de>LibrettistIn</de><en>Librettist</en></role>
        <role code="ldr"><de>Laborleitung</de><en>Laboratory director</en></role>
        <role code="led"><de>Führung</de><en>Lead</en></role>
        <role code="lee"><de>Libelee-appellee</de><en>Libelee-appellee</en></role>
        <role code="lel"><de>BeklagteR im Seerecht/Kirchenrecht</de><en>Libelee</en></role>
        <role code="len"><de>LeihgeberIn</de><en>Lender</en></role>
        <role code="let"><de>Libelee-appellant</de><en>Libelee-appellant</en></role>
        <role code="lgd"><de>LichtgestalterIn</de><en>Lighting designer</en></role>
        <role code="lie"><de>Libelant-appellee</de><en>Libelant-appellee</en></role>
        <role code="lil"><de>KlägerIn im Seerecht/Kirchenrecht</de><en>Libelant</en></role>
        <role code="lit"><de>Libelant-appellant</de><en>Libelant-appellant</en></role>
        <role code="lsa"><de>LandschaftsarchitektIn</de><en>Landscape architect</en></role>
        <role code="lse"><de>LizenznehmerIn</de><en>Licensee</en></role>
        <role code="lso"><de>LizenzgeberIn</de><en>Licensor</en></role>
        <role code="ltg"><de>LithographIn</de><en>Lithographer</en></role>
        <role code="lyr"><de>TextdichterIn</de><en>Lyricist</en></role>
        <role code="mcp"><de>ArrangeurIn</de><en>Music copyist</en></role>
        <role code="mdc"><de>Metadatenkontakt</de><en>Metadata contact</en></role>
        <role code="med"><de>Medium</de><en>Medium</en></role>
        <role code="mfp"><de>Herstellungsort</de><en>Manufacture place</en></role>
        <role code="mfr"><de>HerstellerIn</de><en>Manufacturer</en></role>
        <role code="mod"><de>ModeratorIn</de><en>Moderator</en></role>
        <role code="mon"><de>BeobachterIn</de><en>Monitor</en></role>
        <role code="mrb"><de>MarmorarbeiterIn</de><en>Marbler</en></role>
        <role code="mrk"><de>Markup-EditorIn</de><en>Markup editor</en></role>
        <role code="msd"><de>MusikalischeR LeiterIn</de><en>Musical director</en></role>
        <role code="mte"><de>Metall-GraveurIn</de><en>Metal-engraver</en></role>
        <role code="mtk"><de>ProtokollantIn</de><en>Minute taker</en></role>
        <role code="mus"><de>MusikerIn</de><en>Musician</en></role>
        <role code="nrt"><de>ErzählerIn</de><en>Narrator</en></role>
        <role code="opn"><de>GegnerIn</de><en>Opponent</en></role>
        <role code="org"><de>UrheberIn</de><en>Originator</en></role>
        <role code="orm"><de>VeranstalterIn</de><en>Organizer</en></role>
        <role code="osp"><de>On-screen PräsentatorIn</de><en>Onscreen presenter</en></role>
        <role code="oth"><de>WeitereR MitwirkendeR</de><en>Other</en></role>
        <role code="own"><de>EigentümerIn</de><en>Owner</en></role>
        <role code="pan"><de>DiskussionsteilnehmerIn</de><en>Panelist</en></role>
        <role code="pat"><de>AuftraggeberIn</de><en>Patron</en></role>
        <role code="pbd"><de>Verlagsleitung</de><en>Publishing director</en></role>
        <role code="pbl"><de>Verlag</de><en>Publisher</en></role>
        <role code="pdr"><de>Projektleitung</de><en>Project director</en></role>
        <role code="pfr"><de>KorrektorIn</de><en>Proofreader</en></role>
        <role code="pht"><de>FotografIn</de><en>Photographer</en></role>
        <role code="plt"><de>DruckformherstellerIn</de><en>Platemaker</en></role>
        <role code="pma"><de>Genehmigungsstelle</de><en>Permitting agency</en></role>
        <role code="pmn"><de>Produktionsleitung</de><en>Production manager</en></role>
        <role code="pop"><de>PlattendruckerIn</de><en>Printer of plates</en></role>
        <role code="ppm"><de>PapierherstellerIn / Papiermühle</de><en>Papermaker</en></role>
        <role code="ppt"><de>PuppenspielerIn</de><en>Puppeteer</en></role>
        <role code="pra"><de>Praeses</de><en>Praeses</en></role>
        <role code="prc"><de>Prozesskontakt</de><en>Process contact</en></role>
        <role code="prd"><de>Produktionspersonal</de><en>Production personnel</en></role>
        <role code="pre"><de>PräsentatorIn</de><en>Presenter</en></role>
        <role code="prf"><de>DarstellerIn</de><en>Performer</en></role>
        <role code="prg"><de>ProgrammiererIn</de><en>Programmer</en></role>
        <role code="prm"><de>DruckgrafikerIn</de><en>Printmaker</en></role>
        <role code="prn"><de>Produktionsfirma</de><en>Production company</en></role>
        <role code="pro"><de>ProduzentIn</de><en>Producer</en></role>
        <role code="prp"><de>Produktionsort</de><en>Production place</en></role>
        <role code="prs"><de>SzenenbildnerIn</de><en>Production designer</en></role>
        <role code="prt"><de>DruckerIn</de><en>Printer</en></role>
        <role code="prv"><de>AnbieterIn</de><en>Provider</en></role>
        <role code="pta"><de>PatentanwärterIn</de><en>Patent applicant</en></role>
        <role code="pte"><de>KlägerIn-BerufungsbeklagteR</de><en>Plaintiff-appellee</en></role>
        <role code="ptf"><de>ZivilklägerIn</de><en>Plaintiff</en></role>
        <role code="pth"><de>PatentinhaberIn</de><en>Patent holder</en></role>
        <role code="ptt"><de>KlägerIn-BerufungsklägerIn</de><en>Plaintiff-appellant</en></role>
        <role code="pup"><de>Erscheinungsort</de><en>Publication place</en></role>
        <role code="rbr"><de>RubrikatorIn</de><en>Rubricator</en></role>
        <role code="rcd"><de>TonmeisterIn</de><en>Recordist</en></role>
        <role code="rce"><de>ToningenieurIn</de><en>Recording engineer</en></role>
        <role code="rcp"><de>AdressatIn</de><en>Addressee</en></role>
        <role code="rdd"><de>HörfunkregisseurIn</de><en>Radio director</en></role>
        <role code="red"><de>RedakteurIn</de><en>Redaktor</en></role>
        <role code="ren"><de>RendererIn (Bildverarbeitung)</de><en>Renderer</en></role>
        <role code="res"><de>ForscherIn</de><en>Researcher</en></role>
        <role code="rev"><de>RezensentIn</de><en>Reviewer</en></role>
        <role code="rpc"><de>HörfunkproduzentIn</de><en>Radio producer</en></role>
        <role code="rps"><de>Aufbewahrungsort</de><en>Repository</en></role>
        <role code="rpt"><de>ReporterIn</de><en>Reporter</en></role>
        <role code="rpy"><de>Verantwortliche Partei</de><en>Responsible party</en></role>
        <role code="rse"><de>AntragsgegnerIn-BerufungsbeklagteR</de><en>Respondent-appellee</en></role>
        <role code="rsg"><de>RegisseurIn der Wiederaufführung</de><en>Restager</en></role>
        <role code="rsp"><de>RespondentIn</de><en>Respondent</en></role>
        <role code="rsr"><de>RestauratorIn</de><en>Restorationist</en></role>
        <role code="rst"><de>AntragsgegnerIn-BerufungsklägerIn</de><en>Respondent-appellant</en></role>
        <role code="rth"><de>Leitung des Forschungsteams</de><en>Research team head</en></role>
        <role code="rtm"><de>Mitglied des Forschungsteams</de><en>Research team member</en></role>
        <role code="sad"><de>WissenschaftlicheR BeraterIn</de><en>Scientific advisor</en></role>
        <role code="sce"><de>DrehbuchautorIn</de><en>Scenarist</en></role>
        <role code="scl"><de>BildhauerIn</de><en>Sculptor</en></role>
        <role code="scr"><de>SchreiberIn</de><en>Scribe</en></role>
        <role code="sds"><de>TongestalterIn</de><en>Sound designer</en></role>
        <role code="sec"><de>SekretärIn</de><en>Secretary</en></role>
        <role code="sgd"><de>BühnenregisseurIn</de><en>Stage director</en></role>
        <role code="sgn"><de>UnterzeichnerIn</de><en>Signer</en></role>
        <role code="sht"><de>Unterstützender Veranstalter</de><en>Supporting host</en></role>
        <role code="sll"><de>VerkäuferIn</de><en>Seller</en></role>
        <role code="sng"><de>SängerIn</de><en>Singer</en></role>
        <role code="spk"><de>SprecherIn</de><en>Speaker</en></role>
        <role code="spn"><de>SponsorIn</de><en>Sponsor</en></role>
        <role code="spy"><de>Zweite Partei</de><en>Second party</en></role>
        <role code="srv"><de>LandvermesserIn</de><en>Surveyor</en></role>
        <role code="std"><de>BühnenbildnerIn</de><en>Set designer</en></role>
        <role code="stg"><de>Kulisse</de><en>Setting</en></role>
        <role code="stl"><de>GeschichtenerzählerIn</de><en>Storyteller</en></role>
        <role code="stm"><de>InszenatorIn</de><en>Stage manager</en></role>
        <role code="stn"><de>Normungsorganisation</de><en>Standards body</en></role>
        <role code="str"><de>StereotypeurIn</de><en>Stereotyper</en></role>
        <role code="tcd"><de>Technische Leitung</de><en>Technical director</en></role>
        <role code="tch"><de>AusbilderIn</de><en>Teacher</en></role>
        <role code="ths"><de>BetreuerIn (Doktorarbeit)</de><en>Thesis advisor</en></role>
        <role code="tld"><de>FernsehregisseurIn</de><en>Television director</en></role>
        <role code="tlp"><de>FernsehproduzentIn</de><en>Television producer</en></role>
        <role code="trc"><de>TranskribiererIn</de><en>Transcriber</en></role>
        <role code="trl"><de>ÜbersetzerIn</de><en>Translator</en></role>
        <role code="tyd"><de>Schrift-DesignerIn</de><en>Type designer</en></role>
        <role code="tyg"><de>SchriftsetzerIn</de><en>Typographer</en></role>
        <role code="uvp"><de>Hochschulort</de><en>University place</en></role>
        <role code="vac"><de>SynchronsprecherIn</de><en>Voice actor</en></role>
        <role code="vdg"><de>BildregisseurIn</de><en>Videographer</en></role>
        <role code="wac"><de>KommentarverfasserIn</de><en>Writer of added commentary</en></role>
        <role code="wal"><de>VerfasserIn von zusätzlichen Lyrics</de><en>Writer of added lyrics</en></role>
        <role code="wam"><de>AutorIn des Begleitmaterials</de><en>Writer of accompanying material</en></role>
        <role code="wat"><de>VerfasserIn von Zusatztexten</de><en>Writer of added text</en></role>
        <role code="wdc"><de>HolzschneiderIn</de><en>Woodcutter</en></role>
        <role code="wde"><de>HolzschnitzerIn</de><en>Wood engraver</en></role>
        <role code="win"><de>VerfasserIn einer Einleitung</de><en>Writer of introduction</en></role>
        <role code="wit"><de>ZeugeIn</de><en>Witness</en></role>
        <role code="wpr"><de>VerfasserIn eines Vorworts</de><en>Writer of preface</en></role>
        <role code="wst"><de>VerfasserIn von ergänzendem Text</de><en>Writer of supplementary textual content</en></role>
    </xsl:variable>

</xsl:stylesheet>