FROM gradle:8.8-jdk21-alpine
WORKDIR /app
ADD . .
RUN gradle --no-daemon --no-scan --no-build-cache distTar
RUN cd build/distributions && tar xf app.tar

FROM eclipse-temurin:21-jdk-alpine
COPY --from=0 /app/build/distributions/app /app
CMD /app/bin/xml-data-transform