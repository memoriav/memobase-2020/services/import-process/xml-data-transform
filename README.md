## XML Data Transform

[Confluence Doku](https://memobase.atlassian.net/wiki/spaces/TBAS/pages/29229669/Service+XML+Data+Transform)

This services uses and XSLT transformation config to transform any XML structure into a flat JSON structure.
The goal is to have at most an object structure.

Each record hast to be in a separate XML at the moment as they cannot be separated.

The transformer needs to know the identifierFieldName and the root xml tag. These are used to determine the scope of the XML which is to be transformed.

It fails if there is no identifier as it is not possible to proceed without one.


#### Configuration

- `SFTP_HOST`: The host of the sftp server.
- `SFTP_PORT`: The port of the sftp server.
- `SFTP_USER`: The admin username of the sftp server.
- `SFTP_PASSWORD`: The admin password of the sftp server.
- `REPORTING_STEP_NAME`: The reporting id of this service.
- `SERVICE_CONFIG_TOPIC`: The Kafka topic name which serves the XSLT configurations.
- `KAFKA_BOOTSTRAP_SERVERS`: The Kafka bootstrap server string.
- `APPLICATION_ID`: Unique application id for the Kafka Streams app.
- `TOPIC_IN`: Input topic.
- `TOPIC_OUT`: Output topic.
- `TOPIC_PROCESS`: Reporting topic.
- 
- 