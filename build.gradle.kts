plugins {
    application
    distribution
    jacoco
    kotlin("jvm") version "1.9.24"
    id("io.freefair.git-version") version "6.2.0"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.9.22"
    // id("org.jlleitschuh.gradle.ktlint") version "12.1.1"
    id("org.jetbrains.dokka") version "1.9.20"
}


group = "ch.memobase"

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
}

application {
    mainClass.set("ch.memobase.App")
    tasks.withType<Tar>().configureEach {
        archiveFileName = "app.tar"
    }
}

repositories {
    mavenCentral()
    maven {
        setUrl("https://gitlab.switch.ch/api/v4/projects/1324/packages/maven")
    }
}

dependencies {
    // https://gitlab.switch.ch/memoriav/memobase-2020/libraries/memobase-kafka-utils/-/tags
    implementation("ch.memobase:memobase-kafka-utils:0.3.6")
    // https://gitlab.switch.ch/memoriav/memobase-2020/libraries/service-utilities/-/tags
    implementation("ch.memobase:memobase-service-utilities:4.12.0")

    // https://central.sonatype.com/artifact/org.jetbrains.kotlinx/kotlinx-serialization-json
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")

    // TLS Imports
    // https://central.sonatype.com/artifact/io.github.hakky54/sslcontext-kickstart
    implementation("io.github.hakky54:sslcontext-kickstart:8.3.6")
    // https://central.sonatype.com/artifact/io.github.hakky54/sslcontext-kickstart-for-pem
    implementation("io.github.hakky54:sslcontext-kickstart-for-pem:8.3.6")


    // XSLT Imports
    // https://mvnrepository.com/artifact/net.sf.saxon/Saxon-HE
    implementation("net.sf.saxon:Saxon-HE:12.5")
    // https://mvnrepository.com/artifact/com.ibm.icu/icu4j
    implementation("com.ibm.icu:icu4j:75.1")

    // Logging Imports
    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-api
    implementation("org.apache.logging.log4j:log4j-api:2.23.1")
    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-core
    implementation("org.apache.logging.log4j:log4j-core:2.23.1")
    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-slf4j2-impl
    implementation("org.apache.logging.log4j:log4j-slf4j2-impl:2.23.1")

    // Kafka Imports
    // https://central.sonatype.com/artifact/org.apache.kafka/kafka-streams
    implementation("org.apache.kafka:kafka-streams:3.8.0")

    // SFTP
    // https://central.sonatype.com/artifact/com.hierynomus/sshj
    implementation("com.hierynomus:sshj:0.38.0")

    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter
    testImplementation("org.junit.jupiter:junit-jupiter:5.11.0")
    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter-engine
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.11.0")
    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter-api
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.11.0")
    // https://central.sonatype.com/artifact/org.assertj/assertj-core
    testImplementation("org.assertj:assertj-core:3.25.3")
    // https://central.sonatype.com/artifact/org.apache.kafka/kafka-streams-test-utils
    testImplementation("org.apache.kafka:kafka-streams-test-utils:3.8.0")
}

configurations {
    all {
        exclude(group = "org.slf4j", module = "slf4j-log4j12")
    }
}

tasks.named<Test>("test") {
    useJUnitPlatform()

    testLogging {
        setEvents(mutableListOf("passed", "skipped", "failed"))
    }
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)
    reports {
        csv.required = true
    }
}